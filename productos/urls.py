from rest_framework import routers
from .views import ProductosViewSet
from django.urls import include,path

router = routers.DefaultRouter()
router.register (r'productos', ProductosViewSet, basename='productos')

urlpatterns = [path("", include(router.urls))]
#urlpatterns = router.urls
