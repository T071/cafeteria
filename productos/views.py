from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cafeteriaclase.permissions import IsRecepcionista

class ProductosViewSet(viewsets.ModelViewSet):
    #Minimamente hay que pasar uqeryset y serializer_class
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = [IsRecepcionista] #Instancia y retorna la lista de permisos que esta vista requiere

