from rest_framework import routers
from .views import PedidosViewSet
from django.urls import include,path

router = routers.DefaultRouter()
router.register(r'pedidos', PedidosViewSet, basename='pedidos')

urlpatterns = [
    path("", include(router.urls))
]

#urlpatterns = router.urls